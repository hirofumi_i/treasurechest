@echo off

set git="C:\Program Files\Git\bin\sh.exe"
%git% --norc -c "/bin/ps | /bin/grep -q ssh-agent && /bin/ps | /bin/grep ssh-agent | /bin/gawk '{print $1}' | /bin/xargs /bin/kill"
%git% --norc -c "/bin/mkdir -p /var && /bin/ssh-agent > /var/ssh-agent.out"