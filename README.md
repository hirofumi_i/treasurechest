Treasure Chest
==============

Batch File's like a tresure.

![mimic](https://bytebucket.org/hirofumi_i/treasurechest/raw/af819501d7a03900107dba006131574646c8b69a/img_mimic_01.jpg)


## Target

- Windows 10


## Other documents

- [バッチ ファイルを使用する](https://msdn.microsoft.com/ja-jp/library/cc758944(v=ws.10).aspx)
