cd /d %~dp0
set RUBYOPT=-EUTF-8

rem Watch
::sass --style expanded --sourcemap=none --watch sass:css
::sass --style nested --sourcemap=none --watch sass:css
::sass --style compact --sourcemap=none --watch sass:css
sass --style compressed --sourcemap=none --watch sass:css

rem Add Sourcemap
::sass --style expanded --watch sass:css
::sass --style nested --watch sass:css
::sass --style compact --watch sass:css
::sass --style compressed --watch sass:css